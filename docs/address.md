# Address API Spec

## Create 
Endpoint : POST /api-v1/contacts/{contact_id}/addresses

Headers:
- Authorization : token 

Request Body : 
``` json 
{
    "street" : "jalan ceremai",
    "city" : "menawar",
    "province" : "nirwana",
    "country" : "wakanda",
    "postal_code" : 1111
}
```

Request Body Success : 
``` json
{
    "data" : {
        "id" : 1,
        "street" : "jalan ceremai",
        "city" : "menawar",
        "province" : "nirwana",
        "country" : "wakanda",
        "postal_code" : 1111
    }
}
```

Request Body Error : 
``` json
{
    "errors" : "Country is required"
}
```

## Update 
Endpoint : PUT /api-v1/contacts/{contact_id}/addesses/{address_id}

Headers:
- Authorization : token 
  
Request Body : 
``` json 
{
    "street" : "jalan ceremai",
    "city" : "menawar",
    "province" : "nirwana",
    "country" : "wakanda",
    "postal_code" : 1111
}

```

Request Body Success : 
``` json
{
     "data" : {
        "id" : 1,
        "street" : "jalan ceremai",
        "city" : "menawar",
        "province" : "nirwana",
        "country" : "wakanda",
        "postal_code" : 1111
    }
}
```

Request Body Error : 
``` json
{
    "errors" : "Country is required"
}
```

## Get
Endpoint : GET /api-v1/contacts/{contact_id}/addesses/{address_id}

Headers:
- Authorization : token 


Request Body Success : 
``` json
{
    "data" : {
        "id" : 1,
        "street" : "jalan ceremai",
        "city" : "menawar",
        "province" : "nirwana",
        "country" : "wakanda",
        "postal_code" : 1111
    }
}
```

Request Body Error : 
``` json
{
    "errors" : "contact not fount"
}

```


## List 
Endpoint : GET /api-v1/contacts/{contact_id}/addresses

Headers:
- Authorization : token 

Request Body Success : 
``` json
{
    "data":[
        {
            "id" : 1,
            "street" : "jalan ceremai",
            "city" : "menawar",
            "province" : "nirwana",
            "country" : "wakanda",
            "postal_code" : 1111
        }
    ]
}
```

Request Body Error : 
``` json
{
    "errors" : "contact not fount"
}
```


## Remove
Endpoint : DELETE /api-v1/contacts/{contact_id}/addresses/{address_id}

Headers:
- Authorization : token 

Request Body Success : 
``` json
{
    "data" : "OK"
}
```

Request Body Error : 
``` json
{
    "errors" : "address not fount"
}
```