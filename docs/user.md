# API SPEC

## Register User API
Endpoint : POST /api-v1/users
Request Body :
``` json
{
    "username"  : "yudhaaja",
    "password"  : "12345",
    "name"      : "yudha"
}
```
Response Body Success : 
``` json 
{
    "data" : {
        "username"  : "yudhaaja",
        "password"  : "12345",
        "name"      : "yudha"
    }
}
```
Response Body Error : 
``` json 
{
    "errors" : "username already registered"
}
```

## Login User API
Endpoint : POST /api-v1/users/login
Request Body :
``` json
{
    "username" : "yudhaaja",
    "password" : "12345"
}
```

Response Body Success : 
``` json 
{
    "data" : {
        "token" : "unique-token"
    }
}
```
Response Body Error : 
``` json
{
    "error" : "username or password wrong"
}
```

## Update User API
Endpoint : PATCH /api-v1/users/{id}

- Header 
  Autorization : token
Request Body :
``` json
{
    "name" : "bagus", // optional
    "password" : "password" // optional
}
```
Response Body Success : 
``` json 
{
    "data" : {
        "name"      : "bagus",
        "password"  : "password",
    }
}
```

Response Body Error : 
``` json 
{
    "Error": "Max User length"
}
```
## Get User API
Endpoint : GET /api-v1/users/{id}

Headers :
- Authorization : token

Response Body Success
``` json
{
    "data" : {
        "username"  : "yudhaaja",
        "name"      : "yudha",
    }
}
```

Response Body Error :
``` json
{
    "errors" : "Unauthorized"
}
```

## Logout User API 
Endpoint : DELETE /api-v1/users/logout

Headers :
- Authorization : token
  
Response Body Success :
``` json
{
    "data" : "ok"
}
```

Response Body Error :
``` json
{
    "errors" : "Unauthorized"
}
```