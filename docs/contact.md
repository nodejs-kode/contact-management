# Contact API Spec

## Create 
Endpoint : POST /api-v1/contacts

Headers : 
- Authorization : token

Response Body : 
``` json 
{
    "first_name"    : "yudha",
    "last_name"     : "prasetyo",
    "email"         : "yudha@gmail.com",
    "phone"         : 123456
}
```
  
Response Body Success :
``` json 
{
    "data": {
        "id"            : 1,
        "first_name"    : "yudha",
        "last_name"     : "prasetyo",
        "email"         : "yudha@gmail.com",
        "phone"         : 123456
    }
}
```

Response Body Error : 
``` json
{
    "errors" : "email is not valid format"
}
```

## Update 
Endpoint : PUT /api-v1/contacts/{contact_id}

Headers : 
- Authorization : token

Response Body : 
``` json 
{
    "first_name"    : "bambang",
    "last_name"     : "bambang",
    "email"         : "bambang@gmail.com",
    "phone"         : "098765"
}
```

Response Body Success :
``` json 
{
    "data": {
        "id"            : 1,
        "first_name"    : "bambang",
        "last_name"     : "bambang",
        "email"         : "bambang@gmail.com",
        "phone"         : "098765"
    }
}
```

Response Body Error : 
``` json
{
    "errors" : "email is not valid format"
}
```

## GET
Endpoint : GET /api-v1/contacts/{contact_id}

Headers : 
- Authorization : token

Response Body Success :
``` json 
{
    "data" : {
        "id"            : 1,
        "first_name"    : "bambang",
        "last_name"     : "bambang",
        "email"         : "bambang@gmail.com",
        "phone"         : "098765"
    }
}
```

Response Body Error : 
``` json
{
    "errors" : "contact not found"
}
```

## Search 
Endpoint : GET /api-v1/contacts

Headers : 
- Authorization : token

Qeury params :  
- name : search fist_name or last_name, using like query (optional)
- email : search by email using like, optional
- phone : search by phone using like, optional
- page : number off page, default 1 
- size : size per page, default 10

Response Body Success :
``` json 
{
    "data" :[
        {
            "id"            : 1,
            "first_name"    : "yudha",
            "last_name"     : "prasetyo",
            "email"         : "yudha@gmail.com",
            "phone"         : 123456
        },
        {
            "id"            : 2,
            "first_name"    : "bambang",
            "last_name"     : "bambang",
            "email"         : "bambang@gmail.com",
            "phone"         : "098765"
        }
    ],
    "paging" : {
        "page" : 1,
        "total page" : 3,
        "total_record" : 30
    }
}
```

Response Body Error : 
``` json

```

## Remove 
Endpoint : DELETE /api-v1/contacts/{contact_id}

Headers : 
- Authorization : token


Response Body Success :
``` json 
{
    "data" : "OK"
}
```

Response Body Error : 
``` json
{
    "errors" : "contact not found"
}
```