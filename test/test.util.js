import prisma from "../src/db/database.mjs"
import bcryp from "bcrypt"

const removeTestUser = async () => {
    await prisma.user.deleteMany({
        where : {
            username : "test"
        }
    })
}

const createTestUser = async () => {
     await prisma.user.create({
        data: {
            username: "test",
            password: await bcryp.hash("12345", 10),
            name : "test",
            token : "test"
        }
     })
}
const removeTestContact = async () => {
    await prisma.contact.deleteMany({
        where : {
            username : "test"
        }
    })
}

export {
    removeTestUser,
    createTestUser,
    removeTestContact,
}