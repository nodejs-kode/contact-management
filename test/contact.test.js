import supertest from "supertest";
import web from "../src/app/web.mjs";
import logger from "../src/app/logging.mjs";
import { createTestUser, removeTestUser, createTestContact, removeTestContact } from "./test.util";

describe('POST /api-v1/contact/create', () => { 
    beforeEach(async () => {
        await createTestUser()
    })

    afterEach(async () => {
        await removeTestContact()
        await removeTestUser()
    })
    it('can create contact', async() => {
        const result = await supertest(web)
            .post('/api-v1/contact/create')
            .set('Authorization', 'test')
            .send({
                first_name : "test",
                last_name : "test",
                email : "test@mail.com",
                phone : "1234"
            })
        
        expect(result.status).toBe(200)
        expect(result.body.data.first_name).toBe('test')
        expect(result.body.data.email).toBe('test@mail.com')
    })
    
    it('reject create contact if last name is empty', async() => {
        const result = await supertest(web)
            .post('/api-v1/contact/create')
            .set('Authorization', 'test')
            .send({
                first_name : "test",
                email : "test@mail.com",
                phone : "1234"
            })
        
        expect(result.status).toBe(400)
        expect(result.body.errors).toBeDefined()
    })

    it('reject create contact if last name is null', async() => {
        const result = await supertest(web)
            .post('/api-v1/contact/create')
            .set('Authorization', 'test')
            .send({
                first_name : "test",
                first_name : null,
                email : "test@mail.com",
                phone : "1234"
            })
        
        expect(result.status).toBe(400)
        expect(result.body.errors).toBeDefined()
    })
 })