import supertest from "supertest";
import web from "../src/app/web.mjs";
import logger from "../src/app/logging.mjs";
import { createTestUser, removeTestUser } from "./test.util";

describe('post /api-v1/users', function(){
    afterEach(async()=>{
        await removeTestUser();
    })
    it('can register new user', async () => {
        const result = await supertest(web)
            .post('/api-v1/users')
            .send({
                username : "test",
                password : "12345",
                name : "test"
            })

        expect(result.status).toBe(200)
        expect(result.body.data.username).toBe("test")
        expect(result.body.data.name).toBe("test")
        expect(result.body.data.password).toBeUndefined();
    })
    it('Duplicate new user', async () => {
        let result = await supertest(web)
            .post('/api-v1/users')
            .send({
                username : "test",
                password : "12345",
                name : "test"
            })

        expect(result.status).toBe(200)
        expect(result.body.data.username).toBe("test")
        expect(result.body.data.name).toBe("test")
        expect(result.body.data.password).toBeUndefined();

        result = await supertest(web)
            .post('/api-v1/users')
            .send({
                username : "test",
                password : "12345",
                name : "test"
            })
        expect(result.status).toBe(400)
        expect(result.body.errors).toBeDefined()
    })

    it('should be can not register new user', async () => {
        const result = await supertest(web)
            .post('/api-v1/users')
            .send({
                username : "",
                password : "",
                name : ""
            })
        
        expect(result.status).toBe(400)
        expect(result.body.errors).toBeDefined()
    })
})

describe('POST /api-v1/login', function(){
    beforeEach(async () => {
        await createTestUser()
    })

    afterEach(async () => {
        await removeTestUser()
    })

    it('should can login', async() => {
        const result = await supertest(web)
            .post('/api-v1/login')
            .send({
                username:"test",
                password: "12345"
            })
            
        

        expect(result.status).toBe(200)
        expect(result.body.data.token).toBeDefined()
        expect(result.body.data.token).not.toBe("test")
    })

    it('should canot login', async() => {
        const result = await supertest(web)
            .post('/api-v1/login')
            .send({
                username:"test",
                password: "123455"
            })
            
        

        expect(result.status).toBe(400)
        expect(result.body.errors).toBeDefined()
    })
})

describe('GET /api-v1/user/current', function() {
    beforeEach(async () => {
        await createTestUser()
    })

    afterEach(async () => {
        await removeTestUser()
    })
    it('can get user current', async() => {
        const result = await supertest(web)
            .get('/api-v1/user/current')
            .set('Authorization', 'test')

        expect(result.status).toBe(200)
        expect(result.body.data.username).toBe('test')
        expect(result.body.data.name).toBe('test')
    })
    it('reject get user when token is invalid', async() => {
        const result = await supertest(web)
            .get('/api-v1/user/current')

        expect(result.status).toBe(401)
        expect(result.body.errors).toBeDefined()
    })
})

describe('PATCH /api-v1/user/current', function () {
    beforeEach(async () => {
        await createTestUser()
    })

    afterEach(async () => {
        await removeTestUser()
    })

    it('can update user', async()=>{
        const result = await supertest(web)
            .patch('/api-v1/user/current')
            .set('Authorization','test')
            .send({
                name : 'yudha',
                password: 'rahasia'
            })
        
        expect(result.status).toBe(200)
        expect(result.body.data.name).toBe('yudha')
    })
    it('reject update user when token is invalid', async() => {
        const result = await supertest(web)
            .patch('/api-v1/user/current')

        expect(result.status).toBe(401)
        expect(result.body.errors).toBeDefined()
    })
})

describe('DELETE /api-v1/user/logout', function(){
    beforeEach(async () => {
        await createTestUser()
    })

    afterEach(async () => {
        await removeTestUser()
    })
    it('can logout', async () => {
        const result = await supertest(web)
            .delete('/api-v1/user/logout')
            .set('Authorization','test') 

        expect(result.status).toBe(200)
        expect(result.body.data).toBe("oke")
    })
    it('reject logout if token invalid', async () => {
        const result = await supertest(web)
            .delete('/api-v1/user/logout')
            .set('Authorization', 'test11t') 
       
        expect(result.status).toBe(401)
        expect(result.body.errors).toBeDefined()
    })
})