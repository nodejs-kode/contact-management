import { Prisma } from "@prisma/client"
import prisma from "../db/database.mjs"
import { ResponseError } from "../error/response-error.mjs"
import { getUserValidation, loginUserValidation, registerUserValidation, updateUserValidation } from "../validation/user-validation.mjs"
import { validate } from "../validation/validation.mjs"
import bcryp from "bcrypt"
import { v4 as uuid } from "uuid";

const register = async (request) => {
    const user = validate(registerUserValidation,request)

    const countUser = await prisma.user.count({
        where:{
            username:user.username
        }
    })

    if (countUser ===1 ){
        throw new ResponseError(400, "Username already exist!");
    }

    user.password = await bcryp.hash(user.password, 10)
    
    return prisma.user.create({
        data:user,
        select:{
            username:true,
            name:true
        }
    })
}

const login = async (req) => {
    const loginRequest = validate(loginUserValidation,req)

    const user = await prisma.user.findUnique({
        where :{ 
            username: loginRequest.username
        },
        select: {
            username: true,
            password: true
        }
    })

    if(!user){
        throw new ResponseError(400, "Username or password wrong")
    }

    const isValidPassword = await bcryp.compare(loginRequest.password, user.password)

    if(!isValidPassword){
        throw new ResponseError(400, "Username or password wrong")
    }

    const token = uuid().toString()

    return prisma.user.update({
        data: {
            token:token
        },
        where: {
            username: user.username
        },
        select:{
            token:true
        }
    })

}

const get = async (username) => {
    username = validate(getUserValidation,username)

    const user = await prisma.user.findUnique({
        where: {
            username: username
        },
        select: {
            username: true,
            name: true
        }
    })

    if(!user) {
        throw new ResponseError(404, 'User is not found')
    }

    return user
}

const update = async (req) => {
    const user = validate(updateUserValidation,req)

    const totalUserInDatabase = await prisma.user.count({
        where:{
            username: user.username
        }
    })

    if(totalUserInDatabase != 1){
        throw new ResponseError(404,'User not found')
    }

    const data = {}

    if(user.name){
        data.name = user.name
    }

    if(user.password){
        data.password = await bcryp.hash(user.password,10)
    }

    return await prisma.user.update({
        where :{
            username : user.username
        },
        data:data,
        select :{
            username: true, 
            name:true
        }
    })
}

const logout = async (username) => {
    username = validate(getUserValidation,username)

    const user = await prisma.user.findUnique({
        where : {
            username: username
        }
    })

    if(!user){
        throw new ResponseError(404, 'user not found!')
    }

    return await prisma.user.update({
        where:{
            username:username
        },
        data : {
            token: null
        },
        select:{
            username : true
        }
    })
}
export default {
    register,
    login,
    get,
    update,
    logout
}