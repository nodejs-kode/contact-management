import logger from "../app/logging.mjs";
import prisma from "../db/database.mjs"
import { createContactValidation } from "../validation/contact-validation.mjs"
import { validate } from "../validation/validation.mjs"
import { v4 as uuidv4 } from 'uuid';

const create = async(user, request) => {
    const contact = validate(createContactValidation,request)
    contact.username = user.username
    
    const result = await prisma.contact.create({
        data: {
            id : uuidv4(),
            first_name: contact.first_name,
            last_name: contact.last_name,
            email: contact.email,
            phone: contact.phone,
            username: contact.username
        },
        select:{
            username: true,
            first_name: true,
            last_name: true,
            email: true,
            phone: true,
        }
    })

    return result
}

export default {
    create,
}