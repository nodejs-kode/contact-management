import userService from "../service/user-service.mjs";

const registerController = async (req, res, next) => {
    try {
        const result = await userService.register(req.body)
        res.status(200).json({
            data: result
        })
    } catch (error) {
        next(error)
    }
}

const loginController = async (req,res, next) => {
    try {
        const result = await userService.login(req.body)
        res.status(200).json({
            data: result
        })
    } catch (e) {
        next(e)
    }
}

const get  = async (req,resp, next) => {
    try {
        const username = req.user.username
        const result = await userService.get(username)

        resp.status(200).json({
            data: result
        })
    } catch (error) {
        next(error)
    }
}

const update = async (req, res, next) => {
    try {
        const username = req.user.username
        const request = req.body 

        request.username = username

        const result = await userService.update(request)

        res.status(200).json({
            data: result
        })
    } catch (error) {
        next(error)
    }
}

const logout = async (req, res, next) => {
    try {
        const username = req.user.username 

        await userService.logout(username)

        res.status(200).json({
            data: "oke"
        })
    } catch (error) {
        next(error)
    }
}

export default {
    registerController,
    loginController,
    get,
    update,
    logout,
}