import contactService  from "../service/contact-service.mjs";

const create = async(req, res, next) => {
    try {
        const user = req.user
        const result = await contactService.create(user,req.body)
        res.status(200).json({
            data: result
        })
    } catch (error) {
        next(error)
    }
}

export default {
    create,
}