import express from "express";
import userController from "../controller/user-controller.mjs";

const publicRouter = express.Router()
publicRouter.post('/api-v1/users', userController.registerController)
publicRouter.post('/api-v1/login', userController.loginController)

export default publicRouter