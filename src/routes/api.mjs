import express  from "express";
import userController from "../controller/user-controller.mjs";
import authMiddleware from "../middleware/auth-middleware.mjs";
import contactController from "../controller/contact-controller.mjs";

const userRouter = new express.Router()

userRouter.use(authMiddleware)
userRouter.get('/api-v1/user/current',userController.get)
userRouter.patch('/api-v1/user/current',userController.update)
userRouter.delete('/api-v1/user/logout',userController.logout)

userRouter.post('/api-v1/contact/create', contactController.create)

export {
    userRouter
}