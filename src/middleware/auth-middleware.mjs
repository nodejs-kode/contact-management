import prisma from "../db/database.mjs"

const authMiddleware = async (req,res, next) => {
    const token = req.get('Authorization')

    if(!token){
        res.status(401).json({
            errors: "Unauthorized"
        })
    }else {
        const user = await prisma.user.findFirst({
            where: {
                token: token 
            },
            select: {
                username: true
            }
        })

        if(!user){
            res.status(401).json({
                errors: "Unauthorized"
            })
        } else {
            req.user = user
            next()
        }
    }
}

export default authMiddleware