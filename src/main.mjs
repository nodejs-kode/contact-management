import logger from "./app/logging.mjs";
import web from "./app/web.mjs";

// const logger = require("./app/logging.js")
// const web = require("./app/web.js")

web.listen(3000, () => {
    logger.info("app start in port 3000")
})